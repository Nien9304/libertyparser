#include "struct.h"
#include <fstream>

void library::generateAllSerialFile(string location){
	ofstream ofs;
	string filename;
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
	cellIt != m_cellList.end(); ++cellIt){
		if(cellIt->m_type == CT_INVALID) continue;
		filename = location+cellIt->m_name+".libps";
		openFile(ofs, filename);
		writeSerialFile(*cellIt, ofs);
		ofs.close();
		if(cellIt->m_type != CT_COMB) continue; //only combination cell simulate the patterns.
		filename = location+cellIt->m_name+".1tfpat";
		openFile(ofs, filename);
		write1tfPatternFile(*cellIt, ofs);
		ofs.close();
		filename = location+cellIt->m_name+".2tfpat";
		openFile(ofs, filename);
		write2tfPatternFile(*cellIt, ofs);
		ofs.close();
		
	}
}
void library::writeSerialFile(const cell &currentCell, ostream &os){
	os << "@name" << SEP << currentCell.m_name << endl;
	os << "@info" << SEP << currentCell.m_footprint 
		<< SEP << currentCell.m_drivingStrength
		<< SEP << CELL_TYPE_NAME[currentCell.m_type] 
		<< endl;
		os << "#input" << SEP << currentCell.m_input.size() << endl;
		for(UDT_STR_PIN_MAP::const_iterator pinIt = currentCell.m_input.begin();
			pinIt != currentCell.m_input.end(); ++pinIt){
			os << pinIt->second.m_name
				<< SEP << pinIt->second.m_inputCapacitance * m_cap_unit
				<< endl;
		}
		os << "#output" << SEP << currentCell.m_output.size() << endl;
		for(UDT_STR_PIN_MAP::const_iterator pinIt = currentCell.m_output.begin();
			pinIt != currentCell.m_output.end(); ++pinIt){
			os << pinIt->second.m_name << SEP << CELL_TYPE_NAME[currentCell.m_type]
			   << SEP << pinIt->second.m_outputMaxCapacitance * m_cap_unit
			   << endl;
		}
		if(currentCell.m_seq.m_clockPin.empty()){
			os << "#clock" << SEP << "0" << endl;
		}
		else{
			os << "#clock" << SEP << "1" 
				<< SEP << currentCell.m_seq.m_clockPin 
				<< SEP << currentCell.m_seq.m_clockActivation
				<< endl;
		}
		if(currentCell.m_seq.m_clearPin.empty()){
			os << "#clear" << SEP << "0" << endl;
		}
		else{
			os << "#clear" << SEP << "1" 
				<< SEP << currentCell.m_seq.m_clearPin 
				<< SEP << currentCell.m_seq.m_clearActivation
				<< endl;
		}
		if(currentCell.m_seq.m_presetPin.empty()){
			os << "#preset" << SEP << "0" << endl;
		}
		else{
			os << "#preset" << SEP << "1" 
				<< SEP << currentCell.m_seq.m_presetPin 
				<< SEP << currentCell.m_seq.m_presetActivation
				<< endl;
		}
		os << "#clps" << SEP << currentCell.m_seq.m_clearPresetValueP << SEP
			<< currentCell.m_seq.m_clearPresetValueN << endl;
		os << "#EOF" << endl;	
}
void library::write1tfPatternFile(const cell &currentCell, ostream &os){
	os << "#input" << SEP << currentCell.m_truthTable.m_inputPin.size() << endl;
	os << "#output" << SEP << currentCell.m_truthTable.m_outputPin.size() << endl;
	os << "#pattern" << SEP << currentCell.m_truthTable.m_truthTable.size() << endl;
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_inputPin.begin();
		strIt != currentCell.m_truthTable.m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_outputPin.begin();
		strIt != currentCell.m_truthTable.m_outputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_truthTable.begin();
		strIt != currentCell.m_truthTable.m_truthTable.end(); ++strIt){
		os << *strIt << endl;
	}
	os << "#EOF" << endl;	
}
void library::write2tfPatternFile(const cell &currentCell, ostream &os){
	os << "#input" << SEP << currentCell.m_truthTable.m_inputPin.size() << endl;
	os << "#output" << SEP << currentCell.m_truthTable.m_outputPin.size() << endl;
	os << "#pattern" << SEP << currentCell.m_truthTable.m_transitionTable.size() << endl;
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_inputPin.begin();
		strIt != currentCell.m_truthTable.m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_outputPin.begin();
		strIt != currentCell.m_truthTable.m_outputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(vector<string>::const_iterator 
		strIt = currentCell.m_truthTable.m_transitionTable.begin();
		strIt != currentCell.m_truthTable.m_transitionTable.end(); ++strIt){
		os << *strIt << endl;
	}
	os << "#EOF" << endl;	
}
void library::writeJson(ostream &os){
	bool flag = false;
	os << '[' << endl;
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
	cellIt != m_cellList.end(); ++cellIt){
		if(flag) os << ",";
		os << "{\"name\":\"" << cellIt->m_name << "\",\"type\":\"" << CELL_TYPE_NAME[cellIt->m_type] << "\"}";
		os << endl;
		flag = true;
	}
	os << ']' << endl;
}
void library::writeCellList(ostream &os){
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
	cellIt != m_cellList.end(); ++cellIt){
		if(cellIt->m_type == CT_INVALID) continue;
		os << cellIt->m_name << endl;
	}
}
void library::writeCombCellList(ostream &os){
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
	cellIt != m_cellList.end(); ++cellIt){
		if(cellIt->m_type == CT_COMB){
			os << cellIt->m_name << endl;
		}
	}
}