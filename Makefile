TARGET = libertyParser
OBJECT = lex.yy.o parser.tab.o analyzer.o truthTable.o main.o node.o output.o common.o config.o
OUTPUT = lex.yy.c parser.tab.c parser.tab.h
LEX=flex
YACC=bison
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
# target for debugging
DEBUG:CCFLAG=-g -DLINUX_ENV -std=c++0x -Wall 
DEBUG:TARGET
#makefile knows the dependency between .o .cpp
struct.h: common.h config.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp
config.o:config.cpp config.h
	$(CC) $(CCFLAG) -c config.cpp

analyzer.o:analyzer.cpp struct.h
	$(CC) $(CCFLAG) -c analyzer.cpp
truthTable.o:truthTable.cpp struct.h
	$(CC) $(CCFLAG) -c truthTable.cpp
main.o:main.cpp parser.tab.h struct.h
	$(CC) $(CCFLAG) -c main.cpp
node.o:node.cpp struct.h
	$(CC) $(CCFLAG) -c node.cpp
output.o:output.cpp struct.h
	$(CC) $(CCFLAG) -c output.cpp
lex.yy.o:lex.yy.c parser.tab.h struct.h
	$(CC) $(CCFLAG) -c lex.yy.c
parser.tab.o:parser.tab.c struct.h
	$(CC) $(CCFLAG) -c parser.tab.c
parser.tab.c parser.tab.h:parser.y struct.h
	$(YACC) -d parser.y
lex.yy.c:lexer.l struct.h
	$(LEX) lexer.l
clean:
	rm -f $(TARGET) $(OBJECT) $(OUTPUT) parser.output
# generate internal description of finite-state machine in parser
debug-parser:
	$(YACC) -v parser.y

