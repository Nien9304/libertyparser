#include "struct.h"
#include <algorithm>

void node::update(){
	switch(m_type){
	case G_XOR:
		m_lchild->update();
		m_rchild->update();
		m_value = m_lchild->m_value ^ m_rchild->m_value;
		break;
	case G_OR:
		m_lchild->update();
		m_rchild->update();
		m_value = m_lchild->m_value | m_rchild->m_value;
		break;
	case G_AND:
		m_lchild->update();
		m_rchild->update();
		m_value = m_lchild->m_value & m_rchild->m_value;
		break;
	case G_NOT:
		m_lchild->update();
		m_value = ~m_lchild->m_value;
		break;
	case G_TIE0:
		m_value = 0;
		break;
	case G_TIE1:
		m_value = 1;
		break;
	case G_PI:
		break;
	default:
		cerr << "Unknown gate type" << GATEFUNC_NAME[m_type];
		exit(1);
	}
}

node* node::reconstruct(UDT_MAP<string, node*> &table){
	UDT_MAP<string, node*>::iterator it = table.find(m_name);
	switch(m_type){
	case G_XOR:
	case G_OR:
	case G_AND:
		m_lchild = m_lchild->reconstruct(table);
		m_rchild = m_rchild->reconstruct(table);
		break;
	case G_NOT:
		m_lchild = m_lchild->reconstruct(table);
		break;
	case G_TIE0:
	case G_TIE1:
		break;
	case G_PI:
		if(it != table.end()){
			delete this;
			return it->second;
		}
		else{
			table[m_name] = this;
		}
		break;
	default:
		cerr << "Unknown gate type" << GATEFUNC_NAME[m_type];
		exit(1);
	}
	return this;
}