#include "struct.h"

bool cellNameFilter(char c){ 
	return !(isdigit(c) || c == '.');
}

bool getSpecialPin(UDT_STR_PIN_MAP &input, node* root, string &name, bool &activation){
	if(root == NULL) return true;
	if(root->m_type == G_NOT){
		if(root->m_lchild->m_type == G_PI){
			name = root->m_lchild->m_name;
			activation = false;
			if(input.find(name) == input.end()) return false;
			return true;
		}
	}
	else if(root->m_type == G_PI){
		name = root->m_name;
		activation = true;
		if(input.find(name) == input.end()) return false;
		return true;
	}
	return false;
};

void library::analyzeAllCell(){
	//Assertion for valid cell
	//1. special pins
	//2. IO constraints
	//3. Naming rule
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
		cellIt != m_cellList.end(); ++cellIt){
		
		//resolve sequential element pins and tri-state
		if( getSpecialPin(cellIt->m_input, cellIt->m_seq.m_clock, 
			cellIt->m_seq.m_clockPin, cellIt->m_seq.m_clockActivation) == false){
			cout << "Warning! cannot extract the clock pin of cell " << cellIt->m_name
				<< ". >> Skipped" << endl;
			cellIt->m_type = CT_INVALID;
			continue;
		}
		if( getSpecialPin(cellIt->m_input, cellIt->m_seq.m_clear, 
			cellIt->m_seq.m_clearPin, cellIt->m_seq.m_clearActivation) == false){
			cout << "Warning! cannot extract the clear pin of cell " << cellIt->m_name
				<< ". >> Skipped" << endl;
			cellIt->m_type = CT_INVALID;
			continue;
		}
		if( getSpecialPin(cellIt->m_input, cellIt->m_seq.m_preset,
			cellIt->m_seq.m_presetPin, cellIt->m_seq.m_presetActivation) == false){
			cout << "Warning! cannot extract the preset pin of cell " << cellIt->m_name
				<< ". >> Skipped" << endl;
			cellIt->m_type = CT_INVALID;
			continue;
		}
		for(UDT_STR_PIN_MAP::iterator pinIt = cellIt->m_output.begin();
		pinIt != cellIt->m_output.end(); ++pinIt){
			if( getSpecialPin(cellIt->m_input, pinIt->second.m_tri, 
				pinIt->second.m_triPin, pinIt->second.m_triActivation) == false){
				cout << "Warning! cannot extract the three-state pin of cell " << cellIt->m_name
					<< ". >> Skipped" << endl;
				cellIt->m_type = CT_INVALID;
				break;
			}
		}
		//check IO
		for(UDT_STR_PIN_MAP::iterator pinIt = cellIt->m_output.begin();
		pinIt != cellIt->m_output.end(); ++pinIt){
			if( !pinIt->second.m_triPin.empty() ){
				cout << "Warning! cell " << cellIt->m_name
					<< " has three-state controller. >> Skipped" << endl;
				cellIt->m_type = CT_INVALID;
				break;
			}
		}
		if( cellIt->m_input.size() == 0 ){
			cout << "Warning! cell " << cellIt->m_name
				<< " dose not have inputs. >> Skipped" << endl;
			cellIt->m_type = CT_INVALID;
			continue;
		}
		if( cellIt->m_output.size() == 0 ){
			cout << "Warning! cell " << cellIt->m_name
				<< " dose not have outputs. >> Skipped" << endl;
			cellIt->m_type = CT_INVALID;
			continue;
		}
        // Derive the driving strength from cell name by using its footprint
        //cellName to strength
		string filtered_str = cellIt->m_name;
		//remove footprint from cell name
		string::size_type pos = filtered_str.find(cellIt->m_footprint);
		if (pos != std::string::npos)
		   filtered_str.erase(pos, cellIt->m_footprint.length());
		//replace 0P5 or 0p5 to 0.5
		replace(filtered_str.begin(), filtered_str.end(), 'P', '.');
		replace(filtered_str.begin(), filtered_str.end(), 'p', '.');
		filtered_str.erase(
			remove_if(filtered_str.begin(), filtered_str.end(), cellNameFilter),
			filtered_str.end());
		sscanf(filtered_str.c_str(), "%f%", &cellIt->m_drivingStrength);
		//cout << cellIt->m_name << " " << filtered_str << " " << cellIt->m_drivingStrength << endl;
		if(cellIt->m_drivingStrength == -1.0 || cellIt->m_footprint == "None") {
			cout << "Warning! cannot extract driving strength of cell " 
				<< cellIt->m_name 
				<< ". >> Use the default value 1.0" << endl;
			cellIt->m_drivingStrength = 1.0;
		}
	}
}
