#include "struct.h"
#include <algorithm>
#include <cmath>

void cell::initializeTable(){
	//1. combine the same PI node in all expression into single node.
	//2. generate a vector points to these nodes.
	UDT_MAP<string, node*> piTable;
	if(m_type == CT_COMB){
		//for output pin
		for(UDT_STR_PIN_MAP::iterator pinIt = m_output.begin();
		pinIt != m_output.end(); ++pinIt){
			pinIt->second.m_expression = pinIt->second.m_expression->reconstruct(piTable);
			m_truthTable.m_outputPin.push_back(pinIt->second.m_name);
			m_truthTable.m_outputPinPtr.push_back(pinIt->second.m_expression);
		}
	}
	else if(m_type == CT_FF || m_type == CT_LATCH){
		//for seq //should exclude ck clear preset pin
		if(m_seq.m_data != NULL){
			m_seq.m_data = m_seq.m_data->reconstruct(piTable);
			m_truthTable.m_outputPin.push_back(PPO_NAME);
			m_truthTable.m_outputPinPtr.push_back(m_seq.m_data);
		}	
	}
	//for input
	for(UDT_STR_PIN_MAP::iterator pinIt = m_input.begin();
	pinIt != m_input.end(); ++pinIt){
		if(piTable.find(pinIt->second.m_name) == piTable.end()){
			cout << "Warning pin " << pinIt->second.m_name << " in cell " 
				<< m_name << " is an unsed input pin." << endl;
			continue;
		}
		m_truthTable.m_inputPin.push_back(pinIt->second.m_name);
		m_truthTable.m_inputPinPtr.push_back(piTable[pinIt->second.m_name]);
	}
	m_truthTable.m_1tfpatternNum = pow(2.0, (int)m_truthTable.m_inputPin.size());
}
void truthTable::logicSimTable(){
	m_truthTable.resize(m_1tfpatternNum);
	for(int i=0; i<m_1tfpatternNum; ++i){
		m_truthTable[i].resize(m_inputPin.size() + m_outputPin.size());
	}

	for(int i=0; i<m_1tfpatternNum; ++i){
		for(int j=0; j<m_inputPin.size(); ++j){
			m_truthTable[i][m_inputPin.size()-j-1] = (((i>>j)&0x1) == 0x1) ? '1' : '0';
			m_inputPinPtr[m_inputPin.size()-j-1]->m_value = (i>>j)&0x1;
		}
		for(int j=0; j<m_outputPin.size(); ++j){
			m_outputPinPtr[j]->update();
			m_truthTable[i][m_inputPin.size()+j] = 
				((m_outputPinPtr[j]->m_value&0x1) == 0x1) ? '1' : '0';
		}
	}
}
void truthTable::constructTransitionTable(){
	string pattern, searchPattern;
	pattern.resize(m_inputPin.size() + m_outputPin.size());
	searchPattern.resize(m_inputPin.size());
	m_possible2tfPatternNum = 0;
	for(int i=0; i<m_1tfpatternNum; ++i){
		for(int j=0; j<m_1tfpatternNum; ++j){
			if(j==i) continue;
			int outputTransition = 0;
			int inputTransition = 0;
			//generate pattern
			for(int k=0; k<pattern.size(); ++k){
				if(m_truthTable[i][k] == '0'){
					if(m_truthTable[j][k] == '0'){
						pattern[k] = 'L';
					}
					else{
						pattern[k] = 'R';
						if(k < m_inputPin.size()){
							++inputTransition;
						}
						else{
							++outputTransition;
						}
					}
				}
				else{
					if(m_truthTable[j][k] == '0'){
						pattern[k] = 'F';
						if(k < m_inputPin.size()){
							++inputTransition;
						}
						else{
							++outputTransition;
						}
					}
					else{
						pattern[k] = 'H';
					}
				}
			}
			if(inputTransition > MAX_INPUT_TRANSITION_COUNT) continue; //Limit the num of input transition
			++m_possible2tfPatternNum;
			//pattern may be stable, simulate it.
			if(outputTransition == 0){
				string value;
				if(inputTransition == 1) continue;
				if(simulatePattern(pattern, searchPattern, 0, value) == false) continue;
			}
			//the pattern is possible transition, add to the list.
			m_transitionTable.push_back(pattern);
		}
	}

}
bool truthTable::simulatePattern(const string &pattern, string &searchPattern, int level, string &value){
	if(level == searchPattern.size()){
		if(value.empty()){
			value = upper_bound(m_truthTable.begin(), m_truthTable.end(), searchPattern)
				->substr(m_inputPin.size());
		}
		else if(upper_bound(m_truthTable.begin(), m_truthTable.end(), searchPattern)
			->substr(m_inputPin.size()) != value){
			return true;
		}
		else{ 
			return false;
		}
	}else{
		switch(pattern[level]){
		case 'R':
		case 'F':
			searchPattern[level] = '0';
			if(simulatePattern(pattern, searchPattern, level+1, value) == true){
				return true;
			}
			searchPattern[level] = '1';
			return simulatePattern(pattern, searchPattern, level+1, value);
		case 'L':
			searchPattern[level] = '0';
			return simulatePattern(pattern, searchPattern, level+1, value);
		case 'H':
			searchPattern[level] = '1';
			return simulatePattern(pattern, searchPattern, level+1, value);
		default:
			exit(-1);
		}
	}
}

void library::evaluateTruthTable(){
	for(UDT_CELL_VEC::iterator cellIt = m_cellList.begin();
	cellIt != m_cellList.end(); ++cellIt){
		if(cellIt->m_type == CT_INVALID) continue;
		if(cellIt->m_type != CT_COMB) continue; //seq part is not finished.
		cellIt->initializeTable();
		cellIt->m_truthTable.logicSimTable();
		cellIt->m_truthTable.constructTransitionTable();
		//Show the 2tf pattern count
		/*
		cout << cellIt->m_name << "\t" << cellIt->m_truthTable.m_transitionTable.size();
		cout << "\t " << cellIt->m_truthTable.m_possible2tfPatternNum << endl;
		*/
	}
}