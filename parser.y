%{
#define YYDEBUG 0
#include "struct.h"
 
int lineNum = 1;
extern bool is_string;
extern bool is_paren;
extern int level;
library g_library;
extern char* yytext;
extern "C"{
	void yyerror(const char *s);
	extern int yylex(void);
}
node* createNode(node* op1, node* op2, GATEFUNC type){
	node *nodePtr = new node();
	nodePtr->m_type = type;
	nodePtr->m_lchild = op1;
	nodePtr->m_rchild = op2;
	return nodePtr;
};

%}
%token LIBRARY
%token CAP_UNIT
%token <m_str>LOAD_UNIT
%token CELL
%token FOOTPRINT
%token STATETABLE
%token FF
%token LATCH
%token SEQ_CLK
%token SEQ_FUN
%token SEQ_CLR
%token SEQ_SET
%token SEQ_CSP
%token SEQ_CSN
%token PIN
%token PIN_CAP
%token PIN_DIR
%token PIN_FUN
%token PIN_MAX_CAP
%token PIN_TRI
%token OP_OR
%token OP_AND
%token OP_XOR
%token OP_NOT
%token OP_NOT2
%token <m_int>CONST_BIT
%token <m_double>CONST_FLOAT
%token MK_LPAREN
%token MK_RPAREN
%token MK_LBRACE
%token MK_RBRACE
%token MK_LBRACKET
%token MK_RBRACKET
%token MK_COMMA
%token MK_COLON
%token MK_SEMICOLON
%token MK_DBLQUOTE
%token MK_DOT
%token <m_str>IDENTIFIER
%token ERROR

%union {
	std::string *m_str;
	int m_int;
	double m_double;
	node *m_node;
	pin *m_pin;
	seq *m_seq;
	cell *m_cell;
};
//%type <m_str> 
//%type <m_int> file library library_list;
//%type <m_cell> cell_list cell;
%type <m_cell> cell_stat;
%type <m_seq> ff latch seq_stat;
%type <m_pin> pin pin_stat;
%type <m_node> expression operand0 operand1 operand2 operand3 operand4;

%%
file:
	library_list{
	}
|	{
		cerr << "Error: empty file." << endl;
		exit(-1);
	}
;
library_list:
	library_list library{
	}
|	library{
	}
;
library:
	LIBRARY IDENTIFIER library_stat{
		cout << "[LibertyParser] " << "Include library " << *$2 << endl;
	}
;
library_stat:
	library_stat CAP_UNIT MK_LPAREN CONST_FLOAT MK_COMMA LOAD_UNIT MK_RPAREN MK_SEMICOLON cell_list{
		if (*$6 == "pf") {
			g_library.m_cap_unit = $4 * 1e-12;
		}
		else if (*$6 == "ff") {
			g_library.m_cap_unit = $4 * 1e-15;
		}
		else {
			cerr << "Error: unrecognized capacitive load unit: " << *$6 << endl;
			exit(-1);
		}
	}
|   {
}
;
cell_list:
	cell_list cell{
	}
|	cell{
	}
;
cell:
	CELL IDENTIFIER cell_stat{
		if(!$3->m_footprint.empty()){
			g_library.m_cellList.push_back(*$3);
			g_library.m_cellList.back().m_name = *$2;
		}
		else{
			cout << "Warning: in cell \"" << *$2 
				<< "\" doesn't have footprint." << endl;
			g_library.m_cellList.push_back(*$3);
			g_library.m_cellList.back().m_name = *$2;
			g_library.m_cellList.back().m_footprint = "None";
		}
		delete $2;
		delete $3;
	}
;
cell_stat:
	cell_stat FOOTPRINT MK_COLON IDENTIFIER MK_SEMICOLON{
		$$ = $1;
		$$->m_footprint = *$4;
		delete $4;
	}
|	cell_stat STATETABLE{
		$$ = $1;
		$$->m_type = CT_INVALID; //statetable does not support
	}
|	cell_stat ff{
		$$ = $1;
		$$->m_type = CT_FF;
		$$->m_seq = *$2;
		delete $2;
	}
|	cell_stat latch{
		$$ = $1;
		$$->m_type = CT_LATCH;
		$$->m_seq = *$2;
		delete $2;
	}
|	cell_stat pin{
		$$ = $1;
		if($2->m_dir == PD_INPUT){
			$$->m_input[$2->m_name] = *$2;
		}
		else if($2->m_dir == PD_OUTPUT){
			$$->m_output[$2->m_name] = *$2;
		}
		else if($2->m_dir == PD_UNKNOWN){
			yyerror("pin direction is not defined");
			exit(-1);
		}
		delete $2;
	}
|	{
		$$ = new cell();
		$$->m_type = CT_COMB;
	}
;
ff:
	FF IDENTIFIER IDENTIFIER seq_stat{
		$$ = $4;
		$$->m_nameP = *$2;
		$$->m_nameN = *$3;
		delete $2;
		delete $3;
	}
;
latch:
	LATCH IDENTIFIER IDENTIFIER seq_stat{
		$$ = $4;
		$$->m_nameP = *$2;
		$$->m_nameN = *$3;
		delete $2;
		delete $3;
	}
;
seq_stat:
	seq_stat SEQ_CLK MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_clock = $4;
	}
|	seq_stat SEQ_FUN MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_data = $4;
	}
|	seq_stat SEQ_CLR MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_clear = $4;
	}
|	seq_stat SEQ_SET MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_preset = $4;
	}
|	seq_stat SEQ_CSP MK_COLON CONST_BIT MK_SEMICOLON{
		$$ = $1;
		$$->m_clearPresetValueP = $4;
	}
|	seq_stat SEQ_CSN MK_COLON CONST_BIT MK_SEMICOLON{
		$$ = $1;
		$$->m_clearPresetValueN = $4;
	}
|	{
		$$ = new seq();
	}
;
pin: 
	PIN IDENTIFIER pin_stat{
		$$ = $3;
		$$->m_name = *$2;
		delete $2;
	}
;
pin_stat:
	pin_stat PIN_CAP MK_COLON CONST_FLOAT MK_SEMICOLON{
		$$ = $1;
		$$->m_inputCapacitance = $4;
	}
|	pin_stat PIN_DIR MK_COLON IDENTIFIER MK_SEMICOLON{
		$$ = $1;
		if(*$4 == "input"){
			$$->m_dir = PD_INPUT;
		}
		else if(*$4 == "output"){
			$$->m_dir = PD_OUTPUT;
		} 
		else if(*$4 == "inout"){
			$$->m_dir = PD_INOUT;
		}
		else if(*$4 == "internal"){
			$$->m_dir = PD_INTERNAL;
		}
		else{
			yyerror("Unknown pin direction");
			exit(-1);
		}
	}
|	pin_stat PIN_FUN MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_expression = $4;
	}
|	pin_stat PIN_TRI MK_COLON expression MK_SEMICOLON{
		$$ = $1;
		$$->m_tri = $4;
	}
|	pin_stat PIN_MAX_CAP MK_COLON CONST_FLOAT MK_SEMICOLON{
		$$ = $1;
		$$->m_outputMaxCapacitance = $4;
		//cout << $4 << endl;
	}
|	{
		$$ = new pin();
		$$->m_expression = NULL;
		$$->m_dir = PD_UNKNOWN;
	}
;
expression:
	operand0{
		$$ = $1;
	}
|	CONST_BIT{
		if($1 == 0){
			$$ = createNode(NULL, NULL, G_TIE0);
		}
		else{ 
			$$ = createNode(NULL, NULL, G_TIE1);
		}
	}
;
operand0:
	operand1{
		$$ = $1;
	}
|	operand0 OP_XOR operand1{
		$$ = createNode($1, $3, G_XOR);
	}
;
operand1:
	operand2{
		$$ = $1;
	}
|	operand1 OP_OR operand2{
		$$ = createNode($1, $3, G_OR);
	}
;
operand2:
	operand3{
		$$ = $1;
	}
|	operand2 operand3{
		$$ = createNode($1, $2, G_AND);
	}
|	operand2 OP_AND operand3{
		$$ = createNode($1, $3, G_AND);
	}
;
operand3:
	operand4{
		$$ = $1;
	}
|	OP_NOT operand4{
		$$ = createNode($2, NULL, G_NOT);
	}
|	operand4 OP_NOT2{
		$$ = createNode($1, NULL, G_NOT);
	}
;
operand4:
	IDENTIFIER{
		$$ = new node();
		$$->m_type = G_PI;
		$$->m_name = *$1;
		delete $1;
	}
|	MK_LPAREN operand0 MK_RPAREN{
		$$ = $2;
	}
;

%%
void yyerror(const char *s){
	std::cerr<< "s p l\t" << is_string << is_paren << level << endl;

	std::cerr<< "Error(line" << lineNum << "): " << s;
	std::cerr<< "\t token: " << yytext << std::endl;
}
