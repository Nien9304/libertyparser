#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"
#include "config.h"

extern int MAX_INPUT_TRANSITION_COUNT;

extern "C" int yyparse (void);

//gate type from podem and its name
enum GATEFUNC {G_PI, G_NOT, G_AND, G_OR, G_XOR, G_TIE0, G_TIE1, G_INVALID };
static const char *GATEFUNC_NAME[] = {"G_PI", "G_NOT", "G_AND", "G_OR", "G_XOR", "G_TIE0", "G_TIE1", "G_INVALID" };
enum CELL_TYPE{CT_COMB, CT_FF, CT_LATCH, CT_INVALID};
static const char *CELL_TYPE_NAME[] = {"CT_COMB", "CT_FF", "CT_LATCH", "CT_INVALID" };
enum PIN_DIRECTION{PD_INPUT, PD_OUTPUT, PD_INOUT, PD_INTERNAL, PD_UNKNOWN};
const string PPO_NAME = "data";


typedef map<string, vector<int> > UDT_STR_VECint_SMAP; //is sorted
struct pin;
//UDT_STR_PIN_MAP is sorted to avoid inconsistent pin order in 1tf/2tf 
//pattern file on different platforms
typedef map<string, pin> UDT_STR_PIN_MAP;
struct cell;
typedef vector<cell> UDT_CELL_VEC;
struct node;
typedef vector<node*> UDT_PNODE_VEC;

struct node{
	node():m_type(G_INVALID){};
	void update();
	node* reconstruct(UDT_MAP<string, node*> &table);

	GATEFUNC m_type;
	string m_name;
	node* m_lchild;
	node* m_rchild;
	int m_value;
};
struct seq{
	seq():m_clock(NULL),m_data(NULL),m_clear(NULL),
		m_preset(NULL),m_clearPresetValueP(-1),m_clearPresetValueN(-1){};
	string m_nameP;
	string m_nameN;
	node *m_clock;
	node *m_data;
	node *m_clear;
	node *m_preset;
	int m_clearPresetValueP;
	int m_clearPresetValueN;
	string m_clockPin;
	string m_clearPin;
	string m_presetPin;
	bool m_clockActivation;
	bool m_clearActivation;
	bool m_presetActivation;
};
struct pin{
	pin():m_tri(NULL){};
	string m_name;
	PIN_DIRECTION m_dir;
	double m_inputCapacitance;
	double m_outputMaxCapacitance;
	node *m_expression;
	//tristate control pin
	node* m_tri;
	string m_triPin;
	bool m_triActivation;
};/*
struct outputPin{
	string m_name;
	node* m_expression;
	int m_gateId;
};*/
struct truthTable{
	void logicSimTable();
	void constructTransitionTable();
	bool simulatePattern(const string &pattern, string &searchPattern, int level, string &value);

	int m_1tfpatternNum;
	int m_possible2tfPatternNum; //for statistic
	vector<string> m_inputPin;
	vector<string> m_outputPin;
	vector<node*> m_inputPinPtr;
	vector<node*> m_outputPinPtr;
	
	vector<string> m_truthTable;
	vector<string> m_transitionTable;
};
struct cell{
	cell():m_drivingStrength(-1.0){};
	void initializeTable();

	truthTable m_truthTable;
	string m_name;
	string m_footprint;
	float m_drivingStrength;
	CELL_TYPE m_type;
	UDT_STR_PIN_MAP m_input;
	UDT_STR_PIN_MAP m_output;
	//seq pins
	seq m_seq;
};
struct library{
	UDT_CELL_VEC m_cellList;
	double m_cap_unit;
	void analyzeAllCell();
	void evaluateTruthTable();
	void generateAllSerialFile(string location);
	void writeSerialFile(const cell &currentCell, ostream &os);
	void write1tfPatternFile(const cell &currentCell, ostream &os);
	void write2tfPatternFile(const cell &currentCell, ostream &os);
	void writeJson(ostream &os);
	void writeCellList(ostream &os);
	void writeCombCellList(ostream &os);
};

#endif