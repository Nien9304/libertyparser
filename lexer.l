%{
#define	YY_NO_UNISTD_H 1
#ifndef LINUX_ENV
	#include <io.h>
#else
	#include <unistd.h>
#endif
#include "struct.h"
#include "parser.tab.h"
extern int lineNum;
extern "C"{
	int yywrap(void);
	int yylex(void);
}
enum PARSE_STATE {PS_INIT, PS_LIB, PS_CELL, PS_FF, PS_LATCH, PS_PIN};
PARSE_STATE g_parse_state = PS_INIT;
bool g_read_obj = false;
bool is_string = false;
bool is_paren = false;
int level = 0;
%} 
%x COMMENT_LINE COMMENT_BLOCK

ws              [ \f\r\t\v]+
newline         "\n"
letter          [A-Za-z]
digit           [0-9]
identifier      ("_"|{letter})("_"|{letter}|{digit})*
kw_lib			"library"
kw_cap_unit     "capacitive_load_unit"
load_unit       "pf"
kw_cell			"cell"
kw_footprint    "cell_footprint"
kw_tri			"three_state"
kw_ff			"ff"
kw_latch		"latch"
kw_statetable	"statetable"
kw_ff_clk		"clocked_on"
kw_ff_fun		"next_state"
kw_latch_clk	"enable"
kw_latch_fun	"data_in"
kw_ff_clr		"clear"
kw_ff_set		"preset"
kw_ff_csp		"clear_preset_var1"
kw_ff_csn		"clear_preset_var2"
kw_pin			"pin"
kw_pin_cap		"capacitance"
kw_pin_dir	    "direction"
kw_pin_fun		"function"
kw_pin_max_cap  "max_capacitance"
const_hl		[HLhl]
const_string	"\""[^\"{newline}]*"\""
/*const_bit		[01]*/
const_float		{digit}+"."{digit}+
const_int		{digit}+
op_and			[&*]
op_not			"!"
op_not2			"'"
op_or			[+|]
op_xor			"^"
mk_comment_line	"//"
mk_comment_start "/*"
mk_comment_end  "*/"
mk_lparen       "("
mk_rparen       ")"
mk_lbrace       "{"
mk_rbrace       "}"
mk_lbracket     "["
mk_rbracket     "]"
mk_comma        ","
mk_colon		":"
mk_semicolon    ";"
mk_double_quote	"\""
mk_dot          "."
error            .

%%

{ws}			;   /* do nothing with whitespace */
{mk_comment_line}	{
						BEGIN COMMENT_LINE;
						int i = 0;
						while (yytext[i] != '\0') {
							if (yytext[i] == '\n')
								lineNum++;
							i++;
						}
					}
<COMMENT_LINE>{newline}	{
						BEGIN INITIAL;
						++lineNum;
					}
<COMMENT_LINE>.		;	/* do nothing inside single line comment */
{mk_comment_start}	{
						BEGIN COMMENT_BLOCK;
						int i = 0;
						while (yytext[i] != '\0') {
							if (yytext[i] == '\n')
								lineNum++;
							i++;
						}
					}
<COMMENT_BLOCK>{newline}	{
						++lineNum;
					}
<COMMENT_BLOCK>{mk_comment_end}	{
						BEGIN INITIAL;
					}
<COMMENT_BLOCK>.	;	/* do nothing inside comment block */
{mk_lbrace}		{ ++level; }
{mk_rbrace}		{ 
					--level; 
					if(level == 0){
						switch(g_parse_state){
						case PS_INIT:
							level = 0;
							break;
						case PS_LIB:
							g_parse_state = PS_INIT;
							level = 0;
							break;
						case PS_CELL:
							g_parse_state = PS_LIB;
							level = 1;
							break;
						case PS_FF:
						case PS_LATCH:
						case PS_PIN:
							g_parse_state = PS_CELL;
							level = 1;
							break;
						default:
							cerr << "Unknown lex parse state." << endl;
							exit(-1);
						}
					}
				}
{kw_lib}		{
					if(g_parse_state == PS_INIT && level == 0
					&& !is_string && !is_paren){
						g_parse_state = PS_LIB;
						level = 0;
						return LIBRARY;
					}
				}
{kw_cap_unit}   {
					if(g_parse_state == PS_LIB && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return CAP_UNIT;
					}
				}
{load_unit} {
					if(g_read_obj == true){
						yylval.m_str = new std::string(yytext);
						return LOAD_UNIT;
					} 
}
{kw_cell}		{
					if(g_parse_state == PS_LIB && level == 1
					&& !is_string && !is_paren){
						g_parse_state = PS_CELL;
						level = 0;
						return CELL;
					}
				}
{kw_footprint}	{
					if(g_parse_state == PS_CELL && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return FOOTPRINT;
					}
				}
{kw_statetable}	{
					if(g_parse_state == PS_CELL && level == 1
					&& !is_string && !is_paren){
						//ignore statement inside statetable
						return STATETABLE;
					}
				}
{kw_ff}			{
					if(g_parse_state == PS_CELL && level == 1
					&& !is_string && !is_paren){
						g_parse_state = PS_FF;
						level = 0;
						return FF;
					}
                    else if(g_read_obj == true) {
						yylval.m_str = new std::string(yytext);
                        return LOAD_UNIT;
                    }
				}
{kw_ff_clk}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CLK;
					}
				}
{kw_ff_fun}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_FUN;
					}
				}
{kw_ff_clr}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CLR;
					}
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CLR;
					}
				}
{kw_ff_set}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_SET;
					}
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_SET;
					}
				}
{kw_ff_csp}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CSP;
					}
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CSP;
					}
				}
{kw_ff_csn}		{
					if(g_parse_state == PS_FF && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CSN;
					}
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CSN;
					}
				}
{kw_latch}		{
					if(g_parse_state == PS_CELL && level == 1
					&& !is_string && !is_paren){
						g_parse_state = PS_LATCH;
						level = 0;
						return LATCH;
					}
				}
{kw_latch_clk}	{
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_CLK;
					}
				}
{kw_latch_fun}	{
					if(g_parse_state == PS_LATCH && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return SEQ_FUN;
					}
				}
{kw_pin}		{
					if(g_parse_state == PS_CELL && level == 1
					&& !is_string && !is_paren){
						g_parse_state = PS_PIN;
						level = 0;
						return PIN;
					}
				}
{kw_pin_cap}	{
					if(g_parse_state == PS_PIN && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return PIN_CAP;
					}
				}
{kw_pin_dir}	{
					if(g_parse_state == PS_PIN && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return PIN_DIR;
					}
				}
{kw_pin_fun}	{
					if(g_parse_state == PS_PIN && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return PIN_FUN;
					}
				}
{kw_pin_max_cap} {
					if(g_parse_state == PS_PIN && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return PIN_MAX_CAP;
					}
				}
{kw_tri}		{
					if(g_parse_state == PS_PIN && level == 1
					&& !is_string && !is_paren){
						g_read_obj = true;
						return PIN_TRI;
					}
				}
{const_hl}		{ 
					if(g_read_obj == true){
						if(*yytext == 'L'){
							yylval.m_int = 0;
						}
						else if(*yytext == 'H'){
							yylval.m_int = 1;
						} 
						return CONST_BIT; } 
				}
{identifier}    {
					//cout << "rd_ID " << yytext << endl;
					if((g_parse_state != PS_INIT && level == 0) ||
					g_read_obj == true ){
						yylval.m_str = new std::string(yytext);
						return IDENTIFIER;
					}
				}
{mk_comma}		{ if(g_read_obj == true){ return MK_COMMA; } }
{mk_colon}		{ if(g_read_obj == true){ return MK_COLON; } }
{op_not}		{ if(g_read_obj == true){ return OP_NOT; } }
{op_not2}		{ if(g_read_obj == true){ return OP_NOT2; } }
{op_and}		{ if(g_read_obj == true){ return OP_AND; } }
{op_or}			{ if(g_read_obj == true){ return OP_OR; } }
{op_xor}		{ if(g_read_obj == true){ return OP_XOR; } }
{mk_lparen}		{ if(g_read_obj == true){ return MK_LPAREN; }
					else{is_paren = true;} }
{mk_rparen}		{ if(g_read_obj == true){ return MK_RPAREN; }
					else{is_paren = false;} }
{mk_semicolon}  {
					if(g_read_obj == true){
						g_read_obj = false;
						return MK_SEMICOLON;
					}
				}
{const_int}	    {
                    // As the token const_bit in the order version
					if (g_read_obj == true && is_string) {
                        yylval.m_int = atoi(yytext);
                        return CONST_BIT;
                    }
                    else if (g_read_obj == true) {
						yylval.m_double = atof(yytext);
						return CONST_FLOAT;
                    }
				}
{const_float}	{ 
					if(g_read_obj == true){
						yylval.m_double = atof(yytext);
						return CONST_FLOAT;}
				}
{newline}       {
					++lineNum;
				}
{mk_double_quote}	{
						is_string = !is_string;
					}
.	            {
					//cout << lineNum << "\trd_nothing " << yytext << endl;
					if(g_read_obj == true){
						return ERROR;
					}
				}

%%
int yywrap(void)
{
        return 1;
}
