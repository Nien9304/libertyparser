const char g_message_prefix[] = "[LibertyParser] ";
const char g_version[] = 
//
//	main.cpp
//	LibertyParser
//
//	Created by PCCO	@2015/03/06
//	Maintained by NIEN
"1.5.0"
;
#define MEMORY_DEBUG 0
#if MEMORY_DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include "struct.h"
#include "parser.tab.h"

extern FILE *yyin;
extern library g_library;

string splitFilename(const string& str);
int MAX_INPUT_TRANSITION_COUNT;

int main(int argc, char **argv){
#if MEMORY_DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_crtBreakAlloc = -1;
#endif
	ofstream ofs;
	ifstream ifs;
	string path;
	string filename;
	config configInst;
	if(argc != 4){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./libertyParser "
			 << "[working directory(./)] [liberty file path(*.lib)] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./" << endl;
		//cerr << "Files read from the working directory:" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./result.json (result file)" << endl;
		cerr << "./[cell].libps (cell info file)" << endl;
		cerr << "./[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "./[cell].2tfpat (2tf pattern file)" << endl;
		exit(-1);
	}
	path = argv[1];
	cout << g_message_prefix << "Read config." << endl;
	filename = argv[3];
	openFile(ifs, filename);
	configInst.readConfig(ifs);
	ifs.close();
	configInst.getCfg(string("pat.max_transistion"), MAX_INPUT_TRANSITION_COUNT);
	
	filename = argv[2];
	string libraryName = splitFilename(filename);
	yyin = fopen(filename.c_str(),"r");
	if( yyin == NULL ){
		cerr << "Cannot open " << filename << endl;
		exit(-1);
	}
	cout << g_message_prefix << "Start parsing " << filename << endl;
    if(yyparse() == 1){
		cerr << "Parse error, abort!" << endl;
		exit(-1);
	}
	fclose(yyin);
	g_library.analyzeAllCell();
	cout << g_message_prefix << "Generate tables" << endl;
	g_library.evaluateTruthTable();
	cout << g_message_prefix << "Write cell information file" << endl;
	g_library.generateAllSerialFile(path + '/');
	filename = path + "/" + "result.json";
	cout << g_message_prefix << "write result to " << filename << endl;
	openFile(ofs, filename);
	g_library.writeJson(ofs);
	ofs.close();
	/*filename = path + "/" + libraryName + ".list";
	cout << g_message_prefix << "write cell list to " << filename << endl;
	openFile(ofs, filename);
	g_library.writeCellList(ofs);
	ofs.close();
	filename = path + "/" + libraryName + ".comb.list";
	cout << g_message_prefix << "write cell list to " << filename << endl;
	openFile(ofs, filename);
	g_library.writeCombCellList(ofs);
	ofs.close();*/
    return 0;
} 

string splitFilename(const string& str)
{
	string::size_type found = str.find_last_of("/\\");
	string tmp;
	if(found == string::npos){  
		found = 0;
	}
	else{
		++found;
	}
	tmp = str.substr(found);
	found = tmp.rfind(".lib");
	if(found == string::npos){
		cerr << "Error: The liberty file extension should be *.lib." << endl;
		cerr << "The liberty file path is: " << str << endl;
		cerr << "If you want to disable this error, please modify main.cpp:" 
			<< __LINE__ << endl;
		exit(-1);
	}
	tmp.erase(found);
	return tmp;
}
